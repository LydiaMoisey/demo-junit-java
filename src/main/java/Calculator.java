
public class Calculator {
    private int _a;
    private int _b;

    public Calculator() {
    }

    public Calculator(int a, int b) {
        _a = a;
        _b = b;
    }

    public double sum(double a, double b) {
        double result = a + b;
        return result;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public long multiplication(long a, long b) {
        return a * b;
    }

    public float division(float a, float b) {
        return a / b;
    }

    public int summ() {
        return _a + _b;
    }
    public int sub(){
        return _a - _b;
    }
    public int multiplication(){
        return _a * _b;
    }
    public  int division(){
        return _a / _b;
    }
}
