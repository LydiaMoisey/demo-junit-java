import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum1 (){
      Calculator calc = new Calculator();
      double actual = calc.sum(3, 5.5);
        Assert.assertEquals(8.5, actual, 0.01);
    }
    @Test
    public void testSubtract1 (){
        Calculator calc = new Calculator();
        int actual = calc.subtract(8, 4);
        Assert.assertEquals(4, actual);
    }
    @Test
    public void testMulti1 (){
        Calculator calc = new Calculator();
        long actual = calc.multiplication(50, 50);
        Assert.assertEquals(2500, actual);
    }
    @Test
    public void testDivide1 (){
        Calculator calc = new Calculator();
        float actual = calc.division(100.0f, 20.0f);
        Assert.assertEquals(5f, actual,0.001);
    }
    @Test
    public void testSum2(){
        Calculator calc = new Calculator(11, 6);
        int actual = calc.summ();
        Assert.assertEquals(17, actual);
    }
    @Test
    public void testSub2(){
        Calculator calc = new Calculator(15, 5);
        int actual = calc.sub();
        Assert.assertEquals(10, actual);
    }
    @Test
    public void testMulti2(){
        Calculator calc = new Calculator(5,8);
        int actual = calc.multiplication();
        Assert.assertEquals(40, actual);
    }
    @Test
    public void testDivide2(){
        Calculator calc = new Calculator(50, 25);
        int actual = calc.division();
        Assert.assertEquals(2, actual);
    }
}
